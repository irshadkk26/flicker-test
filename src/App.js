import React from "react";
import logo from "./logo.svg";

import { Row, Col } from "react-simple-flex-grid";
import "react-simple-flex-grid/lib/main.css";

import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormGroup from "@material-ui/core/FormGroup";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";

import TextField from "@material-ui/core/TextField";

import Pagination from "@material-ui/lab/Pagination";
import InputLabel from "@material-ui/core/InputLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      perPage: 10,
      currentpage: 1,
      searchString: "",
    };
  }
  componentDidMount() {
    this.fetchPhotos(20, 1, "mobile");
  }
  fetchPhotos(perPage, currentpage, searchString) {
    this.setState({ isLoaded: false });
    let _url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=5b633b85c13f9c3b0f44bce3aea29603&format=rest&tags=technology&text=${searchString}&content_type=1&per_page=${perPage}&page=${currentpage}`;
    let photoUrlArray = [];
    fetch(_url)
      .then((res) => res.text())
      .then((str) => new window.DOMParser().parseFromString(str, "text/xml"))
      .then(
        (xml) => {
          var photos = xml.getElementsByTagName("photo");
          if (photos) {
            for (var i = 0; i < photos.length; i++) {
              let photo = photos[i];

              let url = `https://farm${photo.getAttribute(
                "farm"
              )}.staticflickr.com/${photo.getAttribute(
                "server"
              )}/${photo.getAttribute("id")}_${photo.getAttribute(
                "secret"
              )}.jpg`;
              // l)
              let isFav = localStorage.getItem(photo) ? true : false;
              photoUrlArray.push({ isFav: isFav, url: url });
            }

            this.setState({
              isLoaded: true,
              photos: photoUrlArray,
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }
  handleSearchChange = (event) => {
    this.setState({ searchString: event.target.value });
    this.fetchPhotos(this.state.perPage, 1, this.state.searchString);
  };
  handlePerpageSelectChange = (event) => {
    console.log(event.target.value);
    this.setState({ perPage: event.target.value });
    this.fetchPhotos(
      parseInt(event.target.value),
      parseInt(this.state.currentpage),
      this.state.searchString
    );
  };
  handleChange = (event, value) => {
    console.log(value);
    this.setState({ currentpage: value });
    this.fetchPhotos(
      parseInt(this.state.perPage),
      parseInt(value),
      this.state.searchString
    );
  };
  handleChangeSearch = (event) => {
    this.setState({ searchString: event.target.value });
  };
  onSearch = (event) => {
    this.fetchPhotos(
      parseInt(this.state.perPage),
      parseInt(this.state.currentpage),
      this.state.searchString
    );
  };
  addToFavourite = (photo) => {
    console.log(photo);
    photo.isFav = !photo.isFav;
    if (photo.isFav) {
      localStorage.setItem(photo.url, photo.url);
    } else {
      localStorage.removeItem(photo.url, photo.url);
    }

    this.setState({ isFavAdded: true });
  };
  handleKeyPress = (event) => {
    console.log("key press");
    console.log(event.key);
    if (event.key === "Enter") {
      console.log('enter pressed')
      this.fetchPhotos(
        parseInt(this.state.perPage),
        parseInt(this.state.currentpage),
        this.state.searchString
      );
    }
  };

  render() {
    return (
      <div className="App">
        <AppBar position="fixed">
          <Toolbar className="header">
            <div className="header-wrapper">
              <Typography variant="h6" className="title">
                Photos
              </Typography>
            </div>
          </Toolbar>
        </AppBar>
        <div className="content-wrapper">
          <Row>
            {this.state.photos.map((photo) => {
              let key = localStorage.getItem(photo.url);
              let className = photo.isFav || key ? "favbtn-selected" : "favbtn";
              return (
                <Col
                  xs={{ span: 12 }}
                  sm={{ span: 6 }}
                  md={{ span: 4 }}
                  lg={{ span: 3 }}
                  xl={{ span: 2 }}
                >
                  <div className="photo-wrapper">
                    <div
                      className="favbtn-wrapper"
                      onClick={this.addToFavourite.bind(this, photo)}
                    >
                      <i
                        tooltip="add to favourite"
                        className={"material-icons " + className}
                      >
                        favorite
                      </i>
                    </div>
                    <div
                      className="photo"
                      style={{ background: `url(${photo.url})` }}
                    ></div>
                  </div>
                </Col>
              );
            })}
          </Row>
        </div>
        <div className="footer-wrapper">
          <div>
            <TextField
              className="textField"
              id="standard-secondary"
              label="Search"
              color="secondary"
              onKeyPress={this.handleKeyPress.bind(this)}
              onChange={this.handleChangeSearch.bind(this)}
            />
          </div>
          {/* <div>
            <IconButton
              onClick={this.onSearch.bind(this)}
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              color="inherit"
            >
              <i className="material-icons">search</i>
            </IconButton>
          </div> */}
          <div>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              onChange={this.handlePerpageSelectChange.bind(this)}
              value={this.state.perPage}
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={50}>Fifty</MenuItem>
            </Select>
          </div>
          <div>
            <Pagination
              className="pagination"
              count={10}
              variant="outlined"
              shape="rounded"
              onChange={this.handleChange.bind(this)}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
