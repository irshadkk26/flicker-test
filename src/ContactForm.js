import React from "react";
import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { ChevronCompactRight, Award } from "react-bootstrap-icons";

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      perPage: 10,
      currentpage: 1,
      searchString: "",
      nameValue: "",
      name: "success",
    };
  }
  componentDidMount() {}
  handleChange(field, event) {
    console.log(event.target.value);
    if (field === "name") {
      this.setState({ nameValue: event.target.value });
    }
  }
  handleSubmit(event) {
    if (this.state.nameValue) {
      this.setState({ name: "success" });
    } else {
      this.setState({ name: "error" });
    }
  }

  render() {
    return (
      <Container>
        <Row className="contact-header">
          <Col xs={2} sm={2} md={2} lg={2} xl={2}>
            {" "}
            <div>
              <span className="logo">F</span> <span>Faby</span>
            </div>
          </Col>
          <Col
            xs={10}
            sm={10}
            md={10}
            lg={10}
            xl={10}
            className="menu-btn-wrapper"
          >
            <Button className="hdbtn" variant="info">
              TOP
            </Button>{" "}
            <Button className="hdbtn" variant="info">
              FEATURES
            </Button>{" "}
            <Button className="hdbtn" variant="info">
              HOW IT WORKS
            </Button>{" "}
            <Button className="hdbtn" variant="info">
              PRICE
            </Button>{" "}
            <Button className="hdbtn" variant="info">
              VIDEO
            </Button>{" "}
            <Button className="active" variant="info">
              GET IT NOW
            </Button>{" "}
          </Col>
        </Row>
        <Row className="content">
          <Col xs={7} sm={7} lg={7} xl={7} md={7}>
            <div className="content-wrapper">
              <div className="content-header">
                <h1>Working Contact Form</h1>
              </div>
              <div className="content-desc">
                <span>
                  We have been woring very hard to create the new version of our
                  cource. it comes with lot of new features.Check it out now.
                </span>
              </div>
              <div className="content-button">
                <Button variant="info">
                  Our Prices
                  <ChevronCompactRight size={16} />
                </Button>{" "}
                <Button variant="secondary">
                  Learn More
                  <Award size={16} />
                </Button>{" "}
              </div>
            </div>
          </Col>
          <Col xs={5} sm={5} lg={5} xl={5} md={5}>
            <div className="form-wrapper">
              <h1>Contact Us</h1>
              <span>Fill in the below form to contact us</span>
              <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Control
                    type="text"
                    placeholder="Name"
                    className={this.state.name}
                    onChange={this.handleChange.bind(this, "name")}
                  />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Control type="email" placeholder="email" />
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlInput1">
                  <Form.Control type="text" placeholder="Subject" />
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Control as="textarea" rows="3" placeholder="Message" />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Button
                    onClick={this.handleSubmit.bind(this)}
                    className="sendmsgbtn"
                    variant="info"
                  >
                    Send Message
                  </Button>{" "}
                </Form.Group>
              </Form>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default ContactForm;
